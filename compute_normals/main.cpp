#include <iostream>
#include <vector>
#include <string>
#include <set>

#include <glm/glm.hpp>

#include "npy.h"

using namespace std;

#define FILE_NAME_INDICES "bunny_faces.npy"
#define FILE_NAME_VERTICES "bunny_vertices.npy"

#define FILE_NAME_FACES_NORMALS "face_normals.npy"
#define FILE_NAME_VERTICES_NORMALS "vertex_normals.npy"

#define COORD_SIZE 3

glm::vec3 getCoordByIndex(int index, const vector<double> & verticesData) {
    int vertexXIndex = COORD_SIZE*index;
    return glm::vec3(
        verticesData[vertexXIndex],
        verticesData[vertexXIndex+1],
        verticesData[vertexXIndex+2]);
}

void setCoordByIndex(int index, vector<double> & verticesData,
    const glm::vec3 & coord) {
    int vertexXIndex = COORD_SIZE*index;
    verticesData[vertexXIndex] = coord.x;
    verticesData[vertexXIndex+1] = coord.y;
    verticesData[vertexXIndex+2] = coord.z;
}

void normalizeVertices(vector<double> & verticesData) {
    for(unsigned i=0; i<verticesData.size()/COORD_SIZE; i++) {
        glm::vec3 normal = getCoordByIndex(i, verticesData);
        if(glm::length(normal) > 0) {
            normal = glm::normalize(normal);
            setCoordByIndex(i, verticesData, normal);
        }
    }
}

vector<int> findAdjacentFaces(
    unsigned facefirstVertex,
    const vector<double> & verticesData, const vector<int> & indicesData
) {
    vector<glm::vec3> faceVertices;
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex], verticesData));
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex+1], verticesData));
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex+2], verticesData));

    vector<int> adjFaces;
    for(int i=0; i<indicesData.size(); i+=3) {
        // looks for vertices of the face in the current face
        if(i != facefirstVertex) {
            unsigned findCount = 0;
            for(unsigned j=0; j<3; j++) {
                auto vertex = getCoordByIndex(indicesData[i+j], verticesData);
                if(std::find(faceVertices.begin(), faceVertices.end(), vertex) != faceVertices.end()) {
                    findCount++;
                }
            }

            if(findCount == 2) {
                adjFaces.push_back(i);
            }
        }
    }
    return adjFaces;
}

int getOrientation(glm::vec3 v1, glm::vec3 v2) {
    auto vectProduct = glm::cross(v1, v2);
    glm::mat3x3 matPassage;
    matPassage[0] = vectProduct;
    matPassage[1] = v1;
    matPassage[2] = v2;
    if(glm::determinant(matPassage) > 0)
        return 1;
    else
        return -1;
}

void computeAdjacentFacesNormalsPointSameSide(
    unsigned currentFace, set<unsigned int> & visitedFaces,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData,
    const vector<double> & verticesData, const vector<int> & indicesData
) {
    if(std::find(visitedFaces.begin(), visitedFaces.end(), currentFace) != visitedFaces.end()) {
        // already (or in process) explored
        return;
    }
    visitedFaces.insert(currentFace);

    auto adjFaces = findAdjacentFaces(currentFace, verticesData, indicesData);
    auto currentNormal = getCoordByIndex(currentFace/COORD_SIZE, facesNormalsData);
    auto currentVector = getCoordByIndex(indicesData[currentFace], verticesData)
        - getCoordByIndex(indicesData[currentFace], verticesData);
    for(unsigned i=0; i<adjFaces.size(); i++) {
        int adjFace = adjFaces[i];

        // Get face vertices
        glm::vec3 A = getCoordByIndex(indicesData[adjFace], verticesData);
        glm::vec3 B = getCoordByIndex(indicesData[adjFace+1], verticesData);
        glm::vec3 C = getCoordByIndex(indicesData[adjFace+2], verticesData);

        // Compute face normal
        glm::vec3 v1 = A-B;
        glm::vec3 v2 = A-C;
        glm::vec3 adjNormal = glm::normalize(glm::cross(v1, v2));

        // Set face normal
        facesNormalsData[adjFace] = adjNormal.x;
        facesNormalsData[adjFace+1] = adjNormal.y;
        facesNormalsData[adjFace+2] = adjNormal.z;

        // Set same orientation as current face normal
        int orientationFaces = getOrientation(currentVector, v1);
        int orientationNormals = getOrientation(currentNormal, adjNormal);
        if(orientationFaces == orientationNormals) {
            // To have same orientation for normals,
            // the faces and normals must have opposite vectorial orientation 
            setCoordByIndex(adjFace/COORD_SIZE, facesNormalsData, -adjNormal);
        }

        // compute and set vertices normals
        for(int j=0; j<3; j++) {
            auto vertexCurrentNormal = getCoordByIndex(indicesData[adjFace+j], verticesNormalsData);
            vertexCurrentNormal += adjNormal;
            setCoordByIndex(indicesData[adjFace+j], verticesNormalsData, vertexCurrentNormal);
        }

        computeAdjacentFacesNormalsPointSameSide(
            adjFace,
            visitedFaces,
            facesNormalsData,
            verticesNormalsData,
            verticesData,
            indicesData
        );
    }
}

void computeNormals(
    const vector<double> & verticesData, const vector<int> & indicesData,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData
) {
    facesNormalsData.resize(indicesData.size());
    std::fill(facesNormalsData.begin(), facesNormalsData.end(), 0);
    verticesNormalsData.resize(verticesData.size());
    std::fill(verticesNormalsData.begin(), verticesNormalsData.end(), 0);

    set<unsigned int> visitedFaces;
    computeAdjacentFacesNormalsPointSameSide(
        0, visitedFaces,
        facesNormalsData,
        verticesNormalsData,
        verticesData,
        indicesData
    );

    normalizeVertices(verticesNormalsData);
}

void opposeAllVector(
    vector<double> & vertorsData
) {
    for(unsigned i=0; i<vertorsData.size()/COORD_SIZE; i++) {
        auto v = getCoordByIndex(i, vertorsData);
        setCoordByIndex(i, vertorsData, -v);
    }
}

void orientNormalsOutwards(
    const vector<double> & verticesData, const vector<int> & indicesData,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData
) {
    /**Based on divergence theorem.*/

    /**Compute approximation of the surface integral on field (x, y, z).*/
    float faceArea;
    glm::vec3 faceCenter;
    glm::vec3 faceNormal;
    float sum = 0;
    for(unsigned i=0; i<indicesData.size(); i+=3) {
        // Get face vertices
        glm::vec3 A = getCoordByIndex(indicesData[i], verticesData);
        glm::vec3 B = getCoordByIndex(indicesData[i+1], verticesData);
        glm::vec3 C = getCoordByIndex(indicesData[i+2], verticesData);

        auto faceCenter = ((1/3.f)*(A+B+C));
        faceNormal = getCoordByIndex(i, facesNormalsData);
        float cosCAB = glm::dot(C-A, B-A)/(glm::distance(A, C), glm::distance(A, B));
        float sinCAB = glm::sqrt(1-glm::pow(cosCAB, 2));
        faceArea = 0.5f
                *glm::distance(A, B)
                *(glm::distance(A, C)/sinCAB);
        sum += (glm::dot(faceCenter, faceNormal))*faceArea;
    }

    if(sum <= 0) {
        opposeAllVector(facesNormalsData);
        opposeAllVector(verticesNormalsData);
    }
}

int main() {
    vector<unsigned long> settings;
    vector<double> verticesData;
    vector<int> indicesData;
    bool f;

    // Load vertices data
    npy::LoadArrayFromNumpy(
        string(FILE_NAME_VERTICES),
        settings,
        f,
        verticesData
    );
    cout << "Vertices settings : " << settings[0] << ", " << settings[1] << endl;
    settings.clear();

    // Load indices data
    npy::LoadArrayFromNumpy(
        string(FILE_NAME_INDICES),
        settings,
        f,
        indicesData
    );
    cout << "Indices settings : " << settings[0] << ", " << settings[1] << endl << endl;

    cout << "indices data size = " << indicesData.size() << endl;
    cout << "vertices data size = " << verticesData.size() << endl << endl;

    for(int i=0; i<verticesData.size(); i+=3) {
    	glm::vec3 n = glm::vec3(
    		verticesData[i],
    		verticesData[i+1],
    		verticesData[i+2]);
        cout << "Vertex Normal (" << verticesData[i]
            << ", " << verticesData[i+1]
            << ", " << verticesData[i+2]
            << ")" << glm::length(n) << endl;
    }

    // Compute normals all in the same random orientation (outwards or inwards)
    vector<double> facesNormalsData;
    vector<double> verticesNormalsData;
    computeNormals(verticesData, indicesData,
        facesNormalsData, verticesNormalsData);
    // Make normals point outward
    orientNormalsOutwards(verticesData, indicesData, facesNormalsData, verticesNormalsData);

    cout << "normal faces data size = " << facesNormalsData.size() << endl;
    cout << "normal vertices data size = " << verticesNormalsData.size() << endl << endl;

    // for(int i=0; i<facesNormalsData.size(); i+=3) {
    //     cout << "Face Normal (" << facesNormalsData[i]
    //         << ", " << facesNormalsData[i+1]
    //         << ", " << facesNormalsData[i+2]
    //         << ")" << endl;
    // }

    // for(int i=0; i<verticesNormalsData.size(); i+=3) {
    // 	glm::vec3 n = glm::vec3(
    // 		verticesNormalsData[i],
    // 		verticesNormalsData[i+1],
    // 		verticesNormalsData[i+2]);
    //     cout << "Vertex Normal (" << verticesNormalsData[i]
    //         << ", " << verticesNormalsData[i+1]
    //         << ", " << verticesNormalsData[i+2]
    //         << ")" << glm::length(n) << endl;
    // }

    // Save faces normals
    unsigned long faceNormalSettings[] = {facesNormalsData.size()/COORD_SIZE, COORD_SIZE};
    npy::SaveArrayAsNumpy(FILE_NAME_FACES_NORMALS,
        false, 2, faceNormalSettings, facesNormalsData);

    // Save vertices normals
    unsigned long vertexNormalSettings[] = {verticesNormalsData.size()/COORD_SIZE, COORD_SIZE};
    npy::SaveArrayAsNumpy(FILE_NAME_VERTICES_NORMALS,
        false, 2, vertexNormalSettings, verticesNormalsData);
}
