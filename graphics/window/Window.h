#ifndef _WINDOW_
#define _WINDOW_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

namespace window {

class Window {
public:
    void init(unsigned width, unsigned height);

    inline bool shouldClose() {
        return glfwWindowShouldClose(w);
    }
    inline void listenForEvents() {
        glfwPollEvents();
    }
    void handleEvents();
    inline void swapBuffers() {
        glfwSwapBuffers(w);
    }
    inline void end() {
        glfwTerminate();
    }

private:
    GLFWwindow * w;

    void initGlew();
};

}

#endif

