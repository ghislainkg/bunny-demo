#include "Window.h"

namespace window {

void Window::init(unsigned width, unsigned height) {
	glfwInit();
	std::cout << "init glfw" << std::endl;
	w = glfwCreateWindow((int)width, (int)height, "Stanford Bunny", NULL, NULL);
	if(w == NULL) {
		std::cout << "Fail to create window" << std::endl;
		glfwTerminate();
		exit(1);
	}
	std::cout << "create" << std::endl;

	// glfwSetFramebufferSizeCallback(w, (GLFWframebuffersizefun) resiseWindow);

	glfwMakeContextCurrent(w);
	std::cout << "done" << std::endl;
}

void Window::handleEvents() {
	if(glfwGetKey(w, GLFW_KEY_SPACE) == GLFW_PRESS) {
	    glfwSetWindowShouldClose(w, true);
	}
}

}

