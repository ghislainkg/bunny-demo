#include <iostream>

#include "window/Window.h"
#include "renderer/Renderer.h"
#include "renderable-obj/SimpleTrianglesRenderable.h"
#include "renderable-obj/NormalsRenderable.h"

#include "vendors/npy.h"
#include "renderable-obj/computeNormals.h"

int main(int argc, char const *argv[]) {

    std::cout << "START" << std::endl;

    std::vector<unsigned long> settings;
    std::vector<double> verticesData;
    std::vector<double> normalsData;
    std::vector<int> indicesData;
    npy::LoadArrayFromNumpy(
        std::string("bunny_vertices.npy"),
        settings,
        verticesData
    );
    settings.clear();
    npy::LoadArrayFromNumpy(
        std::string("bunny_faces.npy"),
        settings,
        indicesData
    );
    std::cout << "READ BUNNY" << std::endl;
    settings.clear();
    npy::LoadArrayFromNumpy(
        std::string("vertex_normals.npy"),
        settings,
        normalsData
    );

    std::vector<double> normalsVerticesData = getVertexNormalsLinesVertexData(verticesData, normalsData);
    auto verticesAndNormalsData = organize(verticesData, normalsData);

    window::Window win;
    win.init(800, 600);
    std::cout << "CREATE WINDOW" << std::endl;

    renderer::Renderer ren;
    ren.init(800, 600);
    std::cout << "CREATE RENDERER" << std::endl;


    torender::SimpleTrianglesRenderable bunny(
        &verticesAndNormalsData,
        (std::vector<unsigned int>*)&indicesData
    );
    ren.addRenderable(&bunny);
    std::cout << "ADD BUNNY TO RENDERER" << std::endl;

    torender::NormalsRenderable nor(&normalsVerticesData);
    ren.addRenderable(&nor);
    std::cout << "ADD NORMALS TO RENDERER" << std::endl;

    while(!win.shouldClose()) {
    	win.handleEvents();
    	win.swapBuffers();

    	ren.loop();

    	win.listenForEvents();
    }
    win.end();
	return 0;
}
