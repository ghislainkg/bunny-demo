#ifndef _RENDERABLE_
#define _RENDERABLE_

#include "Shader.h"

#include <iostream>
#include <vector>

#include <chrono>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define ERROR_CHECK_EXIT(msg) \
    {int error = glGetError(); \
    if(error != GL_NO_ERROR) { \
        std::cout << msg << " ERROR code : " << error << std::endl; \
        std::cout << gluErrorString(error) << std::endl; \
    }}

namespace renderer {

    class Renderer;

class Renderable {
    friend Renderer;

public:
    Renderable(Shader * shader)
    : isReady(false), shader(shader) {}

protected:
    void draw();

    void initAll();

    /**Init vertex buffer data according to the current data.*/
    void initAllVertexBufferData();
    /**Init indices buffer data according to the current data.*/
    void initAllIndicesBufferData();
    void initBuffers();

    /**Uses glEnableVertexAttribArray() and
        glVertexAttribPointer() to set attributs according to the shader program.*/
    virtual void initVertexAttribs() = 0;

    /**Updates vertex and indices buffer data.
     * This function is called before each draw so
     * actions should be done only if there are actual updates in the data.
     * To send the update to the GPU, updateBuffers() should update the OpenGL buffers.*/
    virtual void updateBuffersData(
        std::vector<double> * vertexBufferData,
        std::vector<unsigned int> * indicesBufferData){}
    virtual std::vector<double> * initialVertexBuffersData() = 0;
    virtual std::vector<unsigned int> * initialIndicesBuffersData() = 0;

    /**Uses glBufferSubData() to update vertex and indices buffer.
     * This function is called before each draw so
     * actions should be perform only if update is needed or 
     * says another way, if vertex data or indices data have been changed by.*/
    virtual void updateBuffers(
        unsigned int vertexBufferObj,
        unsigned int indicesBufferObj) = 0;

    virtual void updateUniforms(const Shader & shader, long timeMillis) = 0;

    void setShader(Shader * shader) {
        this->shader = shader;
    }

    /** If useIndices is false, you should set the vertices count.*/
    inline void setUseIndices(bool useIndices) {
        this->useIndices = useIndices;
    }
    inline void setDrawingMode(GLenum drawingMode) {
        this->drawingMode = drawingMode;
    }
    /** Use when not using indices;*/
    inline void setVerticesCount(unsigned verticesCount) {
        this->verticesCount = verticesCount;
    }

    glm::vec3 computeCenter(
        std::vector<double> * verticesData
    ) {
        glm::vec3 center(0.0);
        unsigned count = verticesData->size()/3;
        for(unsigned i=0; i<count; i+=3) {
            glm::vec3 vertex{
                verticesData->at(i),
                verticesData->at(i+1),
                verticesData->at(i+2)
            };
            center = center + vertex;
        }
        return center*(1.0f/count);
    }

private:

    bool isReady;
    bool useIndices = true;
    GLenum drawingMode = GL_TRIANGLES;
    unsigned verticesCount = 0; // Use when not using indices;

    unsigned int vertexArrayObj;
    unsigned int vertexBufferObj;
    unsigned int indicesBufferObj;

    std::vector<double> * vertexBufferData;
    std::vector<unsigned int> * indicesBufferData;

    /**Create and set openGl objects.*/
    void createGlObjs();

    Shader * shader;
};

}

#endif
