#include "Shader.h"

namespace renderer {

unsigned int compileShader(const char * source, GLenum type) {
	// COMPILE VERTEX SHADER
    unsigned int shader_id;
    shader_id = glCreateShader(type);
    
    glShaderSource(shader_id, 1, &source, NULL);
    glCompileShader(shader_id);
    
    // Errors check
    int success;
    char error_message[512];
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(shader_id, 512, NULL, error_message);
        if(type == GL_VERTEX_SHADER)
            std::cout << "Fail compiling vertex shader " << error_message << std::endl;
        if(type == GL_FRAGMENT_SHADER)
            std::cout << "Fail compiling fragment shader " << error_message << std::endl;
        return 0;
    }

    return shader_id;
}

unsigned int linkProgramShaders(int vertexShader, int fragmentShader) {
	// LINK THEM TOGATHER IN A PROGRAM
    unsigned int program_id;
    program_id = glCreateProgram();

    glAttachShader(program_id, vertexShader);
    glAttachShader(program_id, fragmentShader);
    
    glLinkProgram(program_id);
    
    // Errors check
    int success;
    char error_message[512];
    glGetProgramiv(program_id, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(program_id, 512, NULL, error_message);
        std::cout << "Fail linking shaders " << error_message << std::endl;
        return 0;
    }

    return program_id;
}

Shader::Shader(const char * vertexShaderPath, const char * fragmentShaderPath) {
	
	std::string vertexContent;
	std::string fragmentContent;

	std::ifstream vertexFile;
	std::ifstream fragmentFile;

	vertexFile.open(vertexShaderPath);
	fragmentFile.open(fragmentShaderPath);

	std::stringstream sVertex, sFragment;

	sVertex << vertexFile.rdbuf();
	sFragment << fragmentFile.rdbuf();

	vertexFile.close();
	fragmentFile.close();

	vertexContent = sVertex.str();
	fragmentContent = sFragment.str();

	const char * codeVertex = vertexContent.c_str();
	const char * codeFragment = fragmentContent.c_str();

	int vertexShader = compileShader(codeVertex, GL_VERTEX_SHADER);
    int fragmentShader = compileShader(codeFragment, GL_FRAGMENT_SHADER);
    this->programId = linkProgramShaders(vertexShader, fragmentShader);
    
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Shader::use() const {
	glUseProgram(this->programId);
}

void Shader::setUniform(const std::string &name, bool value) const {
	glUniform1i(glGetUniformLocation(this->programId, name.c_str()), (int)value);
}
void Shader::setUniform(const std::string &name, int value) const {
	glUniform1i(glGetUniformLocation(this->programId, name.c_str()), value);
}
void Shader::setUniform(const std::string &name, float value) const {
	glUniform1f(glGetUniformLocation(this->programId, name.c_str()), value);
}

void Shader::setUniform(const std::string &name, double x, double y, double z) const {
    glUniform3f(glGetUniformLocation(this->programId, name.c_str()), x, y, z);
}

void Shader::setUniform(const std::string &name, double x, double y, double z, double w) const {
    glUniform4f(glGetUniformLocation(this->programId, name.c_str()), x, y, z, w);
}

void Shader::setUniform(const std::string &name, float * mat4) const {
    glUniformMatrix4fv(glGetUniformLocation(this->programId, name.c_str()), 1, false, mat4);
}

}
