#include "Renderable.h"

#define ERROR_CHECK_EXIT(msg) \
    {int error = glGetError(); \
    if(error != GL_NO_ERROR) { \
        std::cout << msg << " ERROR code : " << error << std::endl; \
        std::cout << gluErrorString(error) << std::endl; \
    }}

static long getTimeMillis() {
    auto t = std::chrono::high_resolution_clock::now();
    auto td = std::chrono::duration_cast<std::chrono::duration<long long, std::ratio<1l, 1000l>>>(
        t.time_since_epoch());
    return td.count();
}

namespace renderer {

void Renderable::initAll() {
    this->vertexBufferData = initialVertexBuffersData();
    this->indicesBufferData = initialIndicesBuffersData();

    this->createGlObjs();
    this->initBuffers();

    glBindVertexArray(this->vertexArrayObj);

        glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferObj);
        if(useIndices) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indicesBufferObj);

        initVertexAttribs();

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if(useIndices) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    ERROR_CHECK_EXIT("initAll");
}

void Renderable::draw() {

    if(!isReady) {
        initAll();
        this->isReady = true;
    }

    this->shader->use();
    updateUniforms(*this->shader, getTimeMillis());

    ERROR_CHECK_EXIT("draw 1 ");

    updateBuffersData(this->vertexBufferData, this->indicesBufferData);
    updateBuffers(this->vertexBufferObj, this->indicesBufferObj);

    glBindVertexArray(this->vertexArrayObj);

    if(useIndices) {
        glDrawElements(
            this->drawingMode,
            this->indicesBufferData->size(),
            GL_UNSIGNED_INT,
            0);
    }
    else {
        glDrawArrays(
            this->drawingMode,
            0,
            verticesCount);
    }
    glBindVertexArray(0);
    ERROR_CHECK_EXIT("draw");
}

void Renderable::createGlObjs() {
    glGenVertexArrays(1, &(this->vertexArrayObj));
    glGenBuffers(1, &(this->vertexBufferObj));
    glGenBuffers(1, &(this->indicesBufferObj));
}

void Renderable::initBuffers() {
    initAllVertexBufferData();
    if(useIndices) initAllIndicesBufferData();
}

void Renderable::initAllVertexBufferData() {
    glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferObj);
    glBufferData(GL_ARRAY_BUFFER, this->vertexBufferData->size()*sizeof(double), this->vertexBufferData->data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderable::initAllIndicesBufferData() {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indicesBufferObj);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indicesBufferData->size()*sizeof(int), this->indicesBufferData->data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

}
