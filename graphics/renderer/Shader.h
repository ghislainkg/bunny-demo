#ifndef _SHADER_
#define _SHADER_

#include <GL/glew.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

namespace renderer {

class Shader {
public:
	Shader(const char * vertexShaderPath, const char * fragmentShaderPath);

	void use() const;

	void setUniform(const std::string &name, bool value) const;
	void setUniform(const std::string &name, float value) const;
	void setUniform(const std::string &name, int value) const;
	void setUniform(const std::string &name, double x, double y, double z) const;
	void setUniform(const std::string &name, double x, double y, double z, double w) const;
	void setUniform(const std::string &name, float * mat4) const;

	unsigned int programId;
};

}

#endif