#ifndef _RENDERER_
#define _RENDERER_

#include "Renderable.h"

namespace renderer {

class Renderer {
public:
    Renderer();

    void init(unsigned width, unsigned height);

    void loop();

    void displayGLSettings();

    inline void addRenderable(Renderable * renderable) {this->renderables.push_back(renderable);}

    void handleResize(int width, int height);

private:
    bool isReady;

    std::vector<Renderable*> renderables;

    void initGlew();
    void clearGlobalBuffers();
};

}

#endif
