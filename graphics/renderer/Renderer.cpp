#include "Renderer.h"

namespace renderer {

Renderer::Renderer() {
    isReady = false;
}

void Renderer::init(unsigned width, unsigned height) {
    initGlew();
    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
}

void Renderer::loop() {
    clearGlobalBuffers();
    for(unsigned i=0; i<renderables.size(); i++) {
        auto renderable = renderables[i];
        renderable->draw();
    }
}

void Renderer::initGlew() {
	if(glewInit() != GLEW_OK) {
    	std::cout << "Fail to init GLEW" << std::endl;
    	exit(1);
    }
}

void Renderer::clearGlobalBuffers() {
    glClearColor(0.2f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::displayGLSettings(){
    int nbrMaxAttrib;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nbrMaxAttrib);
    std::cout << "nombre maximum d'attribut par vertex : " << nbrMaxAttrib << std::endl;
}

void handleResize(int width, int height) {
    glViewport(0, 0, width, height);
}

}
