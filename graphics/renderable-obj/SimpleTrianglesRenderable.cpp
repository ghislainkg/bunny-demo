#include "SimpleTrianglesRenderable.h"

namespace torender {

SimpleTrianglesRenderable::SimpleTrianglesRenderable(
        std::vector<double> * vertexBufferData,
        std::vector<unsigned int> * indicesBufferData)
    :
    Renderable(nullptr),
    vertexBufferData(vertexBufferData),
    indicesBufferData(indicesBufferData) {
    
    renderer::Shader * shader = new renderer::Shader(
        "./renderable-obj/simpleTrianglesRenderableShader.vertex",
        "./renderable-obj/simpleTrianglesRenderableShader.fragment");
    setShader(shader);

    setUseIndices(true);
    setDrawingMode(GL_TRIANGLES);

    center = computeCenter(vertexBufferData);
}

void SimpleTrianglesRenderable::initVertexAttribs() {
    // Attribut vec3 mPosition
    glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 6*sizeof(double), (void*)0);
    glEnableVertexAttribArray(0);
    // Attribut vec3 mNormal
    glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 6*sizeof(double), (void*)(3*sizeof(double)));
    glEnableVertexAttribArray(1);
}

void SimpleTrianglesRenderable::updateBuffersData(
    std::vector<double> * vertexBufferData,
    std::vector<unsigned int> * indicesBufferData) { }

std::vector<double> * SimpleTrianglesRenderable::initialVertexBuffersData() {
    return vertexBufferData;
}
std::vector<unsigned int> * SimpleTrianglesRenderable::initialIndicesBuffersData() {
    return indicesBufferData;
}

void SimpleTrianglesRenderable::updateBuffers(
    unsigned int vertexBufferObj,
    unsigned int indicesBufferObj) { }

void SimpleTrianglesRenderable::updateUniforms(
    const renderer::Shader & shader,
    long timeMillis) {
    shader.setUniform("mColor", 0.8, 0.2, 0.2, 1.0);
    
    glm::mat4 modelMat(1.f);
    modelMat = glm::scale(modelMat, {4.f, 4.f, 4.f});
    //modelMat = glm::translate(modelMat, -4.0f*center);
    float angle = (timeMillis%1000000)/1000000.0*360;
    modelMat = glm::rotate(
        modelMat,
        angle, glm::vec3{0.f, 1.f, 0.f});
    shader.setUniform("mModelMatrix", glm::value_ptr(modelMat));
}

}
