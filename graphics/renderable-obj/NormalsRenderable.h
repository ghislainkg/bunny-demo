#ifndef _NORMALS_RENDERABLE_
#define _NORMALS_RENDERABLE_

#include "../renderer/Renderable.h"

namespace torender {

class NormalsRenderable : public renderer::Renderable {

public:
    NormalsRenderable(
        std::vector<double> * vertexBufferData)
    :
        Renderable(nullptr),
        vertexBufferData(vertexBufferData) {
    
        renderer::Shader * shader = new renderer::Shader(
            "./renderable-obj/simpleTrianglesRenderableShader.vertex",
            "./renderable-obj/simpleTrianglesRenderableShader.fragment");
        setShader(shader);
        
        setUseIndices(false);
        setDrawingMode(GL_LINES);
        setVerticesCount(vertexBufferData->size()/3);

        center = computeCenter(vertexBufferData);
    }

    ~NormalsRenderable() {}

    void initVertexAttribs() override {
        // Attribut vec3 mPosition
        glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 3*sizeof(double), (void*)0);
        glEnableVertexAttribArray(0);
    }

    void updateBuffersData(
        std::vector<double> * vertexBufferData,
        std::vector<unsigned int> * indicesBufferData) override { }
    std::vector<double> * initialVertexBuffersData() override {
        //glLineWidth(10);
        return vertexBufferData;
    }
    std::vector<unsigned int> * initialIndicesBuffersData() override {
        return nullptr;
    }

    void updateBuffers(
        unsigned int vertexBufferObj,
        unsigned int indicesBufferObj) override {

        }

    void updateUniforms(const renderer::Shader & shader, long timeMillis) override {
        shader.setUniform("mColor", 0.1, 0.8, 0.2, 1.0);
    
        glm::mat4 modelMat(1.f);
        modelMat = glm::scale(modelMat, {4.f, 4.f, 4.f});
        //modelMat = glm::translate(modelMat, -4.0f*center);
        float angle = (timeMillis%1000000)/1000000.0*360;
        modelMat = glm::rotate(
            modelMat,
            angle, glm::vec3{0.f, 1.f, 0.f});
        shader.setUniform("mModelMatrix", glm::value_ptr(modelMat));
    }

private:
    std::vector<double> * vertexBufferData;

    glm::vec3 center;
};

}

#endif
