#ifndef _COMPUTE_NORMALS_
#define _COMPUTE_NORMALS_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <vector>
#include <set>
#include <iostream>

using namespace std;

#define COORD_SIZE 3

static glm::vec3 getCoordByIndex(int index, const vector<double> & verticesData) {
    int vertexXIndex = COORD_SIZE*index;
    return glm::vec3(
        verticesData[vertexXIndex],
        verticesData[vertexXIndex+1],
        verticesData[vertexXIndex+2]);
}

static void setCoordByIndex(int index, vector<double> & verticesData,
    const glm::vec3 & coord) {
    int vertexXIndex = COORD_SIZE*index;
    verticesData[vertexXIndex] = coord.x;
    verticesData[vertexXIndex+1] = coord.y;
    verticesData[vertexXIndex+2] = coord.z;
}

static void normalizeVertices(vector<double> & verticesData) {
    for(unsigned i=0; i<verticesData.size()/COORD_SIZE; i++) {
        glm::vec3 normal = getCoordByIndex(i, verticesData);
        if(glm::length(normal) > 0) {
            normal = glm::normalize(normal);
            setCoordByIndex(i, verticesData, normal);
        }
    }
}

static vector<unsigned> findAdjacentFaces(
    unsigned facefirstVertex,
    const vector<double> & verticesData, const vector<unsigned int> & indicesData
) {
    vector<glm::vec3> faceVertices;
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex], verticesData));
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex+1], verticesData));
    faceVertices.push_back(getCoordByIndex(indicesData[facefirstVertex+2], verticesData));

    vector<unsigned> adjFaces;
    for(unsigned i=0; i<indicesData.size(); i+=3) {
        // looks for vertices of the face in the current face
        if(i != facefirstVertex) {
            unsigned findCount = 0;
            for(unsigned j=0; j<3; j++) {
                auto vertex = getCoordByIndex(indicesData[i+j], verticesData);
                if(std::find(faceVertices.begin(), faceVertices.end(), vertex) != faceVertices.end()) {
                    findCount++;
                }
            }

            if(findCount == 2) {
                adjFaces.push_back(i);
            }
        }
    }
    return adjFaces;
}

static int getOrientation(glm::vec3 v1, glm::vec3 v2) {
    auto vectProduct = glm::cross(v1, v2);
    glm::mat3x3 matPassage;
    matPassage[0] = vectProduct;
    matPassage[1] = v1;
    matPassage[2] = v2;
    if(glm::determinant(matPassage) > 0)
        return 1;
    else
        return -1;
}

static void computeAdjacentFacesNormalsPointSameSide(
    unsigned currentFace, set<unsigned int> & visitedFaces,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData,
    const vector<double> & verticesData, const vector<unsigned int> & indicesData
) {
    if(std::find(visitedFaces.begin(), visitedFaces.end(), currentFace) != visitedFaces.end()) {
        // already (or in process) explored
        return;
    }
    visitedFaces.insert(currentFace);

    auto adjFaces = findAdjacentFaces(currentFace, verticesData, indicesData);
    auto currentNormal = getCoordByIndex(currentFace/COORD_SIZE, facesNormalsData);
    auto currentVector = getCoordByIndex(indicesData[currentFace], verticesData)
        - getCoordByIndex(indicesData[currentFace], verticesData);
    for(unsigned i=0; i<adjFaces.size(); i++) {
        int adjFace = adjFaces[i];

        // Get face vertices
        glm::vec3 A = getCoordByIndex(indicesData[adjFace], verticesData);
        glm::vec3 B = getCoordByIndex(indicesData[adjFace+1], verticesData);
        glm::vec3 C = getCoordByIndex(indicesData[adjFace+2], verticesData);

        // Compute face normal
        glm::vec3 v1 = A-B;
        glm::vec3 v2 = A-C;
        glm::vec3 adjNormal = glm::normalize(glm::cross(v1, v2));

        // Set face normal
        facesNormalsData[adjFace] = adjNormal.x;
        facesNormalsData[adjFace+1] = adjNormal.y;
        facesNormalsData[adjFace+2] = adjNormal.z;

        // Set same orientation as current face normal
        int orientationFaces = getOrientation(currentVector, v1);
        int orientationNormals = getOrientation(currentNormal, adjNormal);
        if(orientationFaces == orientationNormals) {
            // To have same orientation for normals,
            // the faces and normals must have opposite vectorial orientation 
            setCoordByIndex(adjFace/COORD_SIZE, facesNormalsData, -adjNormal);
        }

        // compute and set vertices normals
        for(int j=0; j<3; j++) {
            auto vertexCurrentNormal = getCoordByIndex(indicesData[adjFace+j], verticesNormalsData);
            vertexCurrentNormal += adjNormal;
            setCoordByIndex(indicesData[adjFace+j], verticesNormalsData, vertexCurrentNormal);
        }

        computeAdjacentFacesNormalsPointSameSide(
            adjFace,
            visitedFaces,
            facesNormalsData,
            verticesNormalsData,
            verticesData,
            indicesData
        );
    }
}

static void computeNormals(
    const vector<double> & verticesData, const vector<unsigned int> & indicesData,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData
) {
    facesNormalsData.resize(indicesData.size());
    std::fill(facesNormalsData.begin(), facesNormalsData.end(), 0);
    verticesNormalsData.resize(verticesData.size());
    std::fill(verticesNormalsData.begin(), verticesNormalsData.end(), 0);

    set<unsigned int> visitedFaces;
    computeAdjacentFacesNormalsPointSameSide(
        0, visitedFaces,
        facesNormalsData,
        verticesNormalsData,
        verticesData,
        indicesData
    );

    normalizeVertices(verticesNormalsData);
}

static void opposeAllVector(
    vector<double> & vertorsData
) {
    for(unsigned i=0; i<vertorsData.size()/COORD_SIZE; i++) {
        auto v = getCoordByIndex(i, vertorsData);
        setCoordByIndex(i, vertorsData, -v);
    }
}

static void orientNormalsOutwards(
    const vector<double> & verticesData, const vector<unsigned int> & indicesData,
    vector<double> & facesNormalsData, vector<double> & verticesNormalsData
) {
    /**Based on divergence theorem.*/

    /**Compute approximation of the surface integral on field (x, y, z).*/
    float faceArea;
    glm::vec3 faceCenter;
    glm::vec3 faceNormal;
    float sum = 0;
    for(unsigned i=0; i<indicesData.size(); i+=3) {
        // Get face vertices
        glm::vec3 A = getCoordByIndex(indicesData[i], verticesData);
        glm::vec3 B = getCoordByIndex(indicesData[i+1], verticesData);
        glm::vec3 C = getCoordByIndex(indicesData[i+2], verticesData);

        auto faceCenter = ((1/3.f)*(A+B+C));
        faceNormal = getCoordByIndex(i, facesNormalsData);
        float cosCAB = glm::dot(C-A, B-A)/(glm::distance(A, C), glm::distance(A, B));
        float sinCAB = glm::sqrt(1-glm::pow(cosCAB, 2));
        faceArea = 0.5f
                *glm::distance(A, B)
                *(glm::distance(A, C)/sinCAB);
        sum += (glm::dot(faceCenter, faceNormal))*faceArea;
    }

    if(sum <= 0) {
        opposeAllVector(facesNormalsData);
        opposeAllVector(verticesNormalsData);
    }
}

vector<double> computeVertexNormals(
    vector<double> & verticesData,
    vector<unsigned int> & indicesData) {
     
    // Compute normals all in the same random orientation (outwards or inwards)
    vector<double> facesNormalsData;
    vector<double> verticesNormalsData;
    computeNormals(verticesData, indicesData,
        facesNormalsData, verticesNormalsData);
    // Make normals point outward
    orientNormalsOutwards(verticesData, indicesData, facesNormalsData, verticesNormalsData);

    return verticesNormalsData;
}

vector<double> getVertexNormalsLinesVertexData(
    vector<double> & verticesData,
    vector<double> & normalsData
) {
    vector<double> result;
    result.resize(verticesData.size()*2);

    for(unsigned i=0; i<verticesData.size()/COORD_SIZE; i+=3) {
        glm::vec3 vertex1 = getCoordByIndex(i, verticesData);
        glm::vec3 normal = getCoordByIndex(i, normalsData);

        // cout << "normal : x=" << normal.x
        //     << " y=" << normal.y
        //     << " z=" << normal.z
        //     << endl;

        glm::vec3 vertex2 = vertex1 + 0.01f*normal;

        // cout << "V1 : x=" << vertex1.x
        //     << " y=" << vertex1.y
        //     << " z=" << vertex1.z

        //     << " ||| V2 : x=" << vertex2.x
        //     << " y=" << vertex2.y
        //     << " z=" << vertex2.z

        //     << std::endl;

        setCoordByIndex(i*2, result, vertex1);
        setCoordByIndex(i*2+1, result, vertex2);
    }
    return result;
}

std::vector<double> organize(
    std::vector<double> & verticesData,
    std::vector<double> & normalsData
) {
    std::vector<double> result;
    result.resize(2*verticesData.size());
    int j = 0;
    for(unsigned i=0; i<verticesData.size()/3; i++) {
        setCoordByIndex(
            j, result,
            getCoordByIndex(i, verticesData));
        j++;
        setCoordByIndex(
            j, result,
            getCoordByIndex(i, normalsData));
        j++;
    }
    return result;
}

#endif
