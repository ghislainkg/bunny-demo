#ifndef _SIMPLE_TRIANGLES_RENDERABLE_
#define _SIMPLE_TRIANGLES_RENDERABLE_

#include "../renderer/Renderable.h"

namespace torender {

class SimpleTrianglesRenderable : public renderer::Renderable {

public:
    SimpleTrianglesRenderable(
        std::vector<double> * vertexBufferData,
        std::vector<unsigned int> * indicesBufferData);

    ~SimpleTrianglesRenderable() {

    }

    void initVertexAttribs() override;

    void updateBuffersData(
        std::vector<double> * vertexBufferData,
        std::vector<unsigned int> * indicesBufferData) override;
    std::vector<double> * initialVertexBuffersData() override;
    std::vector<unsigned int> * initialIndicesBuffersData() override;

    void updateBuffers(
        unsigned int vertexBufferObj,
        unsigned int indicesBufferObj) override;

    void updateUniforms(const renderer::Shader & shader, long timeMillis) override;

private:
    std::vector<double> * vertexBufferData;
    std::vector<unsigned int> * indicesBufferData;

    glm::vec3 center;
};

}

#endif
